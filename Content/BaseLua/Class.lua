
local class = {}

--Note:
--if there only single inherent(good code), we can just use setmetatable(NewClass, ParentClass)

--local SubClass = class(ParentClass)
function class:__call(InParentClass)
    --declare a new class
    local NewClass = {}

    --construct
    function NewClass:new()
        local Obj = {}
        setmetatable(Obj, self)
        return Obj
    end

    --index member from parent class
    setmetatable(NewClass, 
        { 
            __index = InParentClass
        }
    )

    --members are stored in class table
    NewClass.__index = NewClass

    return NewClass
end

--metamethod __call is defined in class
setmetatable(class, class)
class.__index = class

return class