--

local json = require ("json")
local LuaUMGLibrary = {}

function LuaUMGLibrary.ParseWidgetProperty(InWidget, InWidgetTable)
    local WidgetClassName = Unreal.KismetSystemLibrary.GetObjectName(Unreal.GameplayStatics.GetObjectClass(InWidget))

    if WidgetClassName == nil or WidgetClassName == "" then
        return
    end

    LuaUMGLibrary["ParseWidgetProperty_" .. WidgetClassName](InWidget, InWidgetTable)
    
end

function LuaUMGLibrary.ParseSlotProperty(InSlot, InSlotTable)
    local SlotClassName = Unreal.KismetSystemLibrary.GetObjectName(Unreal.GameplayStatics.GetObjectClass(InSlot))

    if SlotClassName == nil or SlotClassName == "" then
        return
    end

    LuaUMGLibrary["ParseSlotProperty_" .. SlotClassName](InSlot, InSlotTable)
    
end

function LuaUMGLibrary.CreateChild(InWidgetTree, InParentPanel, InChildLayout)
    --make sure parent is a panel
    if InParentPanel.AddChild == nil then
        return
    end

    local ChildID = InChildLayout["id"]
    local ChildType = InChildLayout["type"]
    local NewWidget = Unreal.LuaNewObject(InWidgetTree, ChildType, ChildID)
    NewWidget:SetVisibility(InChildLayout['visibility'])
    LuaUMGLibrary.ParseWidgetProperty(NewWidget, InChildLayout)
    local NewSlot = InParentPanel:AddChild(NewWidget)
    LuaUMGLibrary.ParseSlotProperty(NewSlot, InChildLayout)

    local Children = InChildLayout["children"]
    if Children == nil or type(Children) ~= "table" then
        return
    end

    for i, v in ipairs(Children) do
        LuaUMGLibrary.CreateChild(InWidgetTree, NewWidget, v)
    end
end


function LuaUMGLibrary.CreateUIFromJsonFile(InWidgetTree, InFileName)
    local JsonStr = Unreal.LuaScriptFunctionLibrary.LoadFileToString(InFileName)
    local LayoutTable = json.decode(JsonStr)
    local RootWidget = InWidgetTree:GetRootWidget()
    LuaUMGLibrary.CreateChild(InWidgetTree, RootWidget, LayoutTable)
end



--begin parse widget property
function LuaUMGLibrary.SplitString(InString, InSep)
    if InSep == nil then
        InSep = "%s"
    end
    local t={} ; i=1
    for str in string.gmatch(InString, "([^"..InSep.."]+)") do
        t[i] = str
        i = i + 1
    end
    return t
end

function LuaUMGLibrary.ParseWidgetProperty_Button(InWidget, InWidgetTable)
    local FunctionName = InWidgetTable["OnClicked"]
    if FunctionName then
        local func = nil
        local TmpTable = LuaUMGLibrary.SplitString(FunctionName, ".")
        if #TmpTable > 1 then
            func = _G[TmpTable[1]]
            for i = 2, #TmpTable do
                func = func[TmpTable[i]]
            end
        else
            func = _G[FunctionName]
        end

        Unreal.BindDelegate(InWidget:GetOnClicked(), func)
    end
end

function LuaUMGLibrary.ParseWidgetProperty_Border(InWidget, InWidgetTable)

end

function LuaUMGLibrary.ParseWidgetProperty_CanvasPanel(InWidget, InWidgetTable)

end

function LuaUMGLibrary.ParseWidgetProperty_Image(InWidget, InWidgetTable)

end

function LuaUMGLibrary.ParseWidgetProperty_TextBlock(InWidget, InWidgetTable)
    InWidget:SetText(InWidgetTable["text"])
end

--end parse widget property


--begin parse slot property

function LuaUMGLibrary.ParseSlotProperty_CanvasPanelSlot(InSlot, InTable)
    local w = InTable["w"]
    local h = InTable["h"]
    local SlotSize = Unreal.KismetMathLibrary.MakeVector2D(w, h)
    InSlot:SetSize(SlotSize)
    
    local x = InTable["x"]
    local y = InTable["y"]
    local SlotPosition = Unreal.KismetMathLibrary.MakeVector2D(x, y)
    InSlot:SetPosition(SlotPosition)

    if InTable["anchors"] and #InTable["anchors"] == 4 then
        
    end


end

function LuaUMGLibrary.ParseSlotProperty_ButtonSlot(InSlot, InTable)
    local layout = Unreal.LuaMakeStruct("AnchorData")
    local alignment = Unreal.KismetMathLibrary.MakeVector2D(0.5, 0.5)
end

function LuaUMGLibrary.ParseSlotProperty_BorderSlot(InSlot, InTable)

end

function LuaUMGLibrary.ParseSlotProperty_VerticalBoxSlot(InSlot, InTable)

end

--end parse slot property


return LuaUMGLibrary


