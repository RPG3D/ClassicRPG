
local Timer = {}
Timer.TimerList = {}
local UniqueHandle = 0

function Timer.SetTimer(InName, InTotalTime, InInterval, InCompleteCB, InUpdateCB)
    UniqueHandle = UniqueHandle + 1
 
    local NewTimer = {}
    NewTimer.Handle = UniqueHandle
    NewTimer.Name = InName

    NewTimer.TotalTime = InTotalTime
    NewTimer.RemainTime = InTotalTime
    NewTimer.LastUpdate = InTotalTime
    NewTimer.Interval = InInterval

    NewTimer.CompleteCB = InCompleteCB
    NewTimer.UpdateCB = InUpdateCB

    Timer.TimerList[UniqueHandle] = NewTimer

    return UniqueHandle

end

function Timer.ClearTimer(InHandle)
    if(Timer.FindTimer(InHandle)) then
        Timer.TimerList[InHandle] = nil
    else
        print("timer handle invalid, can not ClearTimer: " .. InHandle)
    end
end

function Timer.FindTimer(InHandle)
    for k, v in pairs(Timer.TimerList) do
        if(v.Handle == InHandle) then
            return v
        end
    end

    return nil
end

function Timer.Tick(InDeltaTime)
    if InDeltaTime == nil then
        return
    end

    for k, v in pairs(Timer.TimerList) do

        v.RemainTime = v.RemainTime - InDeltaTime

        if(v.RemainTime <= 0) then
            if(v.CompleteCB) then
                v.CompleteCB(v.Handle, v.Name)
            end

            Timer.TimerList[k] = nil
        else
            if(v.LastUpdate - v.RemainTime >= v.Interval) then
                v.LastUpdate = v.RemainTime
                if(v.UpdateCB) then
                    v.UpdateCB(v.Handle, v.Name, v.RemainTime)
                end
            end
        end

    end

end

return Timer