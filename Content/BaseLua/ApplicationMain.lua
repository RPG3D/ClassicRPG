--this file is the entry file for LuaScript plugin(UE4)

inspect = require("inspect")
Timer = require("Timer")
MainMenuMap = require("MainMenuMap")
LuaUMG = require("LuaUMGLibrary")
class = require("Class")

function Main()

    --初始化重要变量和计时器事件绑定
    GameInstance = Unreal.GetGameInstance()
    PlayerCtrl = Unreal.GameplayStatics.GetPlayerController(GameInstance, 0)
    local Singleton = Unreal.GenericGameAPI.GetRPGSingleton()
    local GameEvent = Singleton:GetGameEvent()
    local handle = Unreal.BindDelegate(GameEvent:GetFrameTick(), Timer.Tick)
    
	print(("Lua Ram: %.2fMB"):format(collectgarbage("count") / 1024))
    
    Unreal.GenericGameAPI.HackNonePakLimit()

    --脚本开始，直接执行主界面地图逻辑
    MainMenuMap.BeginPlay()
end