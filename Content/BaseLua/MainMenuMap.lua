
LoginUILogic = require("UIAsset.LoginUILogic")
class = require("Class")
LuaDbg = require("LuaDebug").start("127.0.0.1",9999)

local MainMenuMap = {}

local function ProcessButtonClick()
    print("OnClick")
end

function MainMenuMap.BeginPlay()
    PlayerCtrl:SetbShowMouseCursor(true)
    MainMenuMap.PlayGameMainMusic()

    Timer.SetTimer("lxq", 3, 1, MainMenuMap.TimerCompleteCB, nil)


    local BpWidgetPath = "/Game/ClientCore/BpWidget.BpWidget_C"
    local BpWidgetClass = Unreal.LuaLoadClass(PlayerCtrl, BpWidgetPath)
    local BpWidgetInst = Unreal.WidgetBlueprintLibrary.Create(PlayerCtrl, BpWidgetClass, PlayerCtrl)
    local BpWidgetTree = BpWidgetInst:GetWidgetTree()
    BpWidgetInst:AddToViewport(0)

    LuaUMG.CreateUIFromJsonFile(BpWidgetTree, Unreal.KismetSystemLibrary.GetProjectContentDirectory() .. "BaseLua/UIAsset/GameLoginUI.json")

    
end

function MainMenuMap.PlayGameMainMusic()
    local MyCharacter = PlayerCtrl:K2_GetPawn()
    --local AbilityComponent = MyCharacter:GetAbilitySystemComponent()
    local AbilitySpecHandle = MyCharacter:GiveAbilityByClassName("/Game/Ability/GA_三柴剑法.GA_三柴剑法_C")
    MyCharacter:TryActiveAbilityByHandle(AbilitySpecHandle)

end

function MainMenuMap.TimerCompleteCB(InHandle, InName)
    print("Timer end")
    Unreal.GameplayStatics.OpenLevel(GameInstance, "/Game/Maps/Login_Map")
end

return MainMenuMap