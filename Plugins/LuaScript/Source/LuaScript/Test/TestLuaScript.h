// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LuaScript.h"
#include "TestLuaScript.generated.h"

/**
 * 
 */
UCLASS()
class LUASCRIPT_API UTestLuaScript : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	DECLARE_DYNAMIC_DELEGATE_OneParam(FOnTestLuaEvent, const FString&, InParam);

	UFUNCTION(BlueprintCallable)
		static int32 TestArrayToCpp(const TArray<FString>& SomeString);
	UFUNCTION(BlueprintCallable)
		static TArray<int32> TestArrayToLua(const TArray<int32>& InParam);

	UFUNCTION(BlueprintCallable)
		static int32 TestMapToCpp(const TMap<int32, FString> InKV);
	UFUNCTION(BlueprintCallable)
		static TMap<int32, FString> TestMapToLua();

	UFUNCTION(BlueprintCallable)
		static int32 TestSetToCpp(const TSet<FString> InStr);
	UFUNCTION(BlueprintCallable)
		static TSet<FString> TestSetToLua();

	UFUNCTION(BlueprintCallable)
		static bool TestNan();

};
