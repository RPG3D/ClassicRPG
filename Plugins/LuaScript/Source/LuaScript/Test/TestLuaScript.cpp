// Fill out your copyright notice in the Description page of Project Settings.

#include "TestLuaScript.h"
#include "UnrealMisc.h"
#include "LuaUnrealWrapper.h"
#include "PlatformFilemanager.h"

int32 UTestLuaScript::TestArrayToCpp(const TArray<FString>& SomeString)
{
	return SomeString.Num();
}

TArray<int32> UTestLuaScript::TestArrayToLua(const TArray<int32>& InParam)
{
	TArray<int32> Ret;
	for (int32 i = InParam.Num() - 1; i >= 0; --i)
	{
		Ret.Add(InParam[i]);
	}

	return Ret;
}

int32 UTestLuaScript::TestMapToCpp(const TMap<int32, FString> InKV)
{
	return InKV.Num();
}

TMap<int32, FString> UTestLuaScript::TestMapToLua()
{
	TMap<int32, FString> Ret;
	Ret.Add(123, FString("Liu"));
	Ret.Add(124, FString(TEXT("ϲ")));
	Ret.Add(125, FString("qiang"));

	return Ret;
}

int32 UTestLuaScript::TestSetToCpp(const TSet<FString> InStr)
{
	return InStr.Num();
}

TSet<FString> UTestLuaScript::TestSetToLua()
{
	TSet<FString> Ret;
	Ret.Add(FString("liu"));
	Ret.Add(FString("xiqiang"));

	return Ret;
}

bool UTestLuaScript::TestNan()
{
	double a = 0.0;
	double Nan = 0.0 / a;
	return Nan == Nan;
}
