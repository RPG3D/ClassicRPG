// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Engine/EngineTypes.h"
#include "LuaScriptConfig.generated.h"

/**
 * 
 */
UCLASS(config = Game)
class LUASCRIPT_API ULuaScriptConfig : public UObject
{
	GENERATED_BODY()
public:
	UPROPERTY(config, EditAnywhere, Category = LuaScript, meta = (DisplayName = "Directory to require lua file(relative path)"))
		TArray<FDirectoryPath> LuaEnvRelativePaths;
	UPROPERTY(config, EditAnywhere, Category = LuaScript, meta = (DisplayName = "Directory to require lua file(full path)"))
		TArray<FDirectoryPath> LuaEnvFullPaths;
	UPROPERTY(config, EditAnywhere, Category = LuaScript, meta = (DisplayName = "Main function defined in the lua file"))
		FFilePath EntryFile;
};
