﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

struct lua_State;

struct FLuaObjectWrapper
{
public:

	UObject * ObjInst = nullptr;

};


struct FLuaStructWrapper
{
public:
	UScriptStruct * StructType = nullptr;
	void* StructInst = nullptr;
};

struct FLuaDelegateWrapper
{
public:
	//FMulticastScriptDelegate(Multi) or FScriptDelegate
	void* DelegateInst = nullptr;
	bool bIsMulti = false;
	UFunction* FunctionSignature = nullptr;
};

/**
 * 
 */
class FUnrealMisc
{
public:
	static bool HasScriptAccessibleField(const UStruct* InStruct);
	static bool IsScriptCallableFunction(const UFunction* InFunction);
	static bool IsScriptReadableProperty(const UProperty* InProperty);

	static void PushProperty(lua_State* InL, UProperty* InProp, void* InBuff, bool bRef = true);
	static void FetchProperty(lua_State* InL, UProperty* InProp, void* InBuff, int32 InStackIndex = -1, FName ErrorName = TEXT(""));

	static UObject* FetchObject(lua_State* InL, int32 InIndex);
	static void PushObject(lua_State* InL, UObject* InObj);

	static void* FetchStruct(lua_State* InL, int32 InIndex);
	static void PushStruct(lua_State* InL, UScriptStruct* InStruct, void* InBuff);

	static int32 GetObjectProperty(lua_State* L);
	static int32 SetObjectProperty(lua_State* L);

	static int32 GetStructProperty(lua_State* InL);
	static int32 SetStructProperty(lua_State* InL);

	static int32 CallFunction(lua_State* L);
	static int32 CallStaticFunction(lua_State* L);
};
