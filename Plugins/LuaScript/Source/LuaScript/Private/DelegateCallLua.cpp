// Fill out your copyright notice in the Description page of Project Settings.

#include "DelegateCallLua.h"
#include "LuaScript.h"
#include "lua.hpp"
#include "UnrealMisc.h"


UDelegateCallLua::UDelegateCallLua(const FObjectInitializer& ObjInit)
	:Super(ObjInit)
{

}

lua_State* UDelegateCallLua::LuaState = nullptr;

void UDelegateCallLua::ProcessEvent(UFunction* InFunction, void* Parms)
{
	if (LuaState == nullptr || FunctionSignature == nullptr)
	{
		return;
	}
	//get lua function in global registry
	lua_rawgeti(LuaState, LUA_REGISTRYINDEX, LuaFunctionID);
	int ParamsNum = 0;
	UProperty* ReturnParam = nullptr;
	for (TFieldIterator<UProperty> It(FunctionSignature); It; ++It)
	{
		//get function return Param
		UProperty* CurrentParam = *It;
		if (CurrentParam->HasAnyPropertyFlags(CPF_ReturnParm))
		{
			ReturnParam = CurrentParam;
		}
		else
		{
			//set params for lua function
			FUnrealMisc::PushProperty(LuaState, CurrentParam, Parms, CurrentParam->HasAnyPropertyFlags(CPF_OutParm));
			++ParamsNum;
		}
	}

	//call lua function
	int32 CallRet = lua_pcall(LuaState, ParamsNum, 0, 0);
	if (CallRet)
	{
		UE_LOG(LogLuaScript, Warning, TEXT("%s"), UTF8_TO_TCHAR(lua_tostring(LuaState, -1)));
	}
	
	if (ReturnParam)
	{
		//get function return Value, in common
		FUnrealMisc::FetchProperty(LuaState, ReturnParam, Parms);
	}
}
