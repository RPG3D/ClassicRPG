﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "UnrealMisc.h"
#include "StructOnScope.h"
#include "CoreUObject.h"
#include "LuaUnrealWrapper.h"
#include "lua.hpp"
#include "LuaScript.h"


int32 CallFunction_Impl(lua_State* L, int32 StackTop, UObject* Obj, UFunction* Func);

bool FUnrealMisc::HasScriptAccessibleField(const UStruct* InStruct)
{
	for (TFieldIterator<const UField> It(InStruct); It; ++It)
	{
		if (const UFunction* Func = Cast<UFunction>(*It))
		{
			if (IsScriptCallableFunction(Func))
			{
				return true;
			}
		}
		else if (const UProperty* Prop = Cast<UProperty>(*It))
		{
			if (IsScriptReadableProperty(Prop))
			{
				return true;
			}
		}
	}


	return false;
}

bool FUnrealMisc::IsScriptCallableFunction(const UFunction* InFunction)
{
	return InFunction && InFunction->HasAnyFunctionFlags(FUNC_BlueprintCallable);
}

bool FUnrealMisc::IsScriptReadableProperty(const UProperty* InProperty)
{
	return InProperty && InProperty->HasAnyPropertyFlags(CPF_BlueprintAssignable | CPF_BlueprintVisible | CPF_InstancedReference) ;
}

void FUnrealMisc::PushProperty(lua_State* InL, UProperty* InProp, void* InBuff, bool bRef /*= true*/)
{
	if (InProp == nullptr)
	{
		UE_LOG(LogLuaScript, Warning, TEXT("Property is nil"));
		return;
	}
	if (UIntProperty* IntProp = Cast<UIntProperty>(InProp))
	{
		lua_pushinteger(InL, IntProp->GetPropertyValue_InContainer(InBuff));
	}
	else if (UFloatProperty* FloatProp = Cast<UFloatProperty>(InProp))
	{
		lua_pushnumber(InL, FloatProp->GetPropertyValue_InContainer(InBuff));
	}
	else if (UEnumProperty* EnumProp = Cast<UEnumProperty>(InProp))
	{
		UNumericProperty* NumProp = EnumProp->GetUnderlyingProperty();
		lua_pushinteger(InL, NumProp->GetSignedIntPropertyValue(InBuff));
	}
	else if (UBoolProperty* BoolProp = Cast<UBoolProperty>(InProp))
	{
		lua_pushboolean(InL, BoolProp->GetPropertyValue_InContainer(InBuff));
	}
	else if (UNameProperty* NameProp = Cast<UNameProperty>(InProp))
	{
		FName name = NameProp->GetPropertyValue_InContainer(InBuff);
		lua_pushstring(InL, TCHAR_TO_UTF8(*name.ToString()));
	}
	else if (UStrProperty* StrProp = Cast<UStrProperty>(InProp))
	{
		const FString& str = StrProp->GetPropertyValue_InContainer(InBuff);
		lua_pushstring(InL, TCHAR_TO_UTF8(*str));
	}
	else if (UTextProperty* TextProp = Cast<UTextProperty>(InProp))
	{
		const FText& text = TextProp->GetPropertyValue_InContainer(InBuff);
		lua_pushstring(InL, TCHAR_TO_UTF8(*text.ToString()));
	}
	else if (UClassProperty* ClassProp = Cast<UClassProperty>(InProp))
	{
		PushObject(InL, ClassProp->GetPropertyValue_InContainer(InBuff));
	}
	else if (UStructProperty* StructProp = Cast<UStructProperty>(InProp))
	{
		if (UScriptStruct* ScriptStruct = Cast<UScriptStruct>(StructProp->Struct))
		{
			PushStruct(InL, ScriptStruct, StructProp->ContainerPtrToValuePtr<void>(InBuff));
		}
	}
	else if (UObjectProperty* ObjectProp = Cast<UObjectProperty>(InProp))
	{
		PushObject(InL, ObjectProp->GetObjectPropertyValue(ObjectProp->GetPropertyValuePtr_InContainer(InBuff)));
	}
	else if (UDelegateProperty* DelegateProp = Cast<UDelegateProperty>(InProp))
	{
		FScriptDelegate* Delegate = DelegateProp->GetPropertyValuePtr_InContainer(InBuff);
		if (Delegate)
		{
			FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_newuserdata(InL, sizeof(FLuaDelegateWrapper));
			Wrapper->bIsMulti = false;
			Wrapper->DelegateInst = Delegate;
			Wrapper->FunctionSignature = DelegateProp->SignatureFunction;
		}
	}
	else if (UMulticastDelegateProperty* MultiDelegateProp = Cast<UMulticastDelegateProperty>(InProp))
	{
		FMulticastScriptDelegate* Delegate = MultiDelegateProp->GetPropertyValuePtr_InContainer(InBuff);
		if (Delegate)
		{
			FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_newuserdata(InL, sizeof(FLuaDelegateWrapper));
			Wrapper->bIsMulti = true;
			Wrapper->DelegateInst = Delegate;
			Wrapper->FunctionSignature = MultiDelegateProp->SignatureFunction;
		}
	}
	else if (UArrayProperty* ArrayProp = Cast<UArrayProperty>(InProp))
	{
		FScriptArrayHelper ArrayHelper(ArrayProp, InBuff);
		lua_newtable(InL);
		for (int32 i = 0; i < ArrayHelper.Num(); ++i)
		{
			PushProperty(InL, ArrayProp->Inner, ArrayHelper.GetRawPtr(i), true);
			lua_rawseti(InL, -2, i);
		}
	}
	else if (USetProperty* SetProp = Cast<USetProperty>(InProp))
	{
		FScriptSetHelper SetHelper(SetProp, InBuff);
		lua_newtable(InL);
		for (int32 i = 0; i < SetHelper.Num(); ++i)
		{
			PushProperty(InL, SetHelper.GetElementProperty(), SetHelper.GetElementPtr(i), true);
			lua_rawseti(InL, -2, i);
		}
	}
	else if (UMapProperty* MapProp = Cast<UMapProperty>(InProp))
	{
		FScriptMapHelper MapHelper(MapProp, InBuff);
		lua_newtable(InL);
		for (int32  i = 0; i < MapHelper.Num(); ++i)
		{
			uint8* PairPtr = MapHelper.GetPairPtr(i);
			PushProperty(InL, MapProp->KeyProp, PairPtr, true);
			PushProperty(InL, MapProp->ValueProp, PairPtr, true);
			lua_rawset(InL, -3);
		}
	}
}

void FUnrealMisc::FetchProperty(lua_State* InL, UProperty* InProp, void* InBuff, int32 InStackIndex /*= -1*/, FName ErrorName /*= TEXT("")*/)
{
	//no enough params
	if (lua_gettop(InL) < lua_absindex(InL, InStackIndex))
	{
		return;
	}

	FString PropName = InProp->GetName();

	if (UIntProperty* IntProp = Cast<UIntProperty>(InProp))
	{
		IntProp->SetPropertyValue_InContainer(InBuff, lua_tointeger(InL, InStackIndex));
	}
	else if (UFloatProperty* FloatProp = Cast<UFloatProperty>(InProp))
	{
		FloatProp->SetPropertyValue_InContainer(InBuff, lua_tonumber(InL, InStackIndex));
	}
	else if (UBoolProperty* BoolProp = Cast<UBoolProperty>(InProp))
	{
		BoolProp->SetPropertyValue_InContainer(InBuff, lua_toboolean(InL, InStackIndex));
	}
	else if (UNameProperty* NameProp = Cast<UNameProperty>(InProp))
	{
		NameProp->SetPropertyValue_InContainer(InBuff, UTF8_TO_TCHAR(lua_tostring(InL, InStackIndex)));
	}
	else if (UStrProperty* StrProp = Cast<UStrProperty>(InProp))
	{
		StrProp->SetPropertyValue_InContainer(InBuff, UTF8_TO_TCHAR(lua_tostring(InL, InStackIndex)));
	}
	else if (UTextProperty* TextProp = Cast<UTextProperty>(InProp))
	{
		TextProp->SetPropertyValue_InContainer(InBuff, FText::FromString(UTF8_TO_TCHAR(lua_tostring(InL, InStackIndex))));
	}
	else if (UByteProperty* ByteProp = Cast<UByteProperty>(InProp))
	{
		ByteProp->SetPropertyValue_InContainer(InBuff, lua_tointeger(InL, InStackIndex));
	}
	else if (UEnumProperty* EnumProp = Cast<UEnumProperty>(InProp))
	{
		UNumericProperty* UnderlyingProp = EnumProp->GetUnderlyingProperty();
		UnderlyingProp->SetIntPropertyValue(EnumProp->ContainerPtrToValuePtr<void>(InBuff), lua_tointeger(InL, InStackIndex));
	}
	else if (UClassProperty* ClassProp = Cast<UClassProperty>(InProp))
	{
		//TODO check property
		ClassProp->SetPropertyValue_InContainer(InBuff, FetchObject(InL, InStackIndex));
	}
	else if (UStructProperty* StructProp = Cast<UStructProperty>(InProp))
	{
		StructProp->Struct->CopyScriptStruct(InBuff, FetchStruct(InL, InStackIndex));
	}
	else if (UObjectProperty* ObjectProp = Cast<UObjectProperty>(InProp))
	{
		//TODO, check property
		ObjectProp->SetObjectPropertyValue_InContainer(InBuff, FetchObject(InL, InStackIndex));
	}
	else if (UDelegateProperty* DelegateProp = Cast<UDelegateProperty>(InProp))
	{
		FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_touserdata(InL, InStackIndex);
		if (Wrapper && Wrapper->DelegateInst && Wrapper->bIsMulti == false)
		{
			DelegateProp->SetPropertyValue_InContainer(InBuff, *(FScriptDelegate*)Wrapper->DelegateInst);
		}
	}
	else if (UMulticastDelegateProperty* MultiDelegateProp = Cast<UMulticastDelegateProperty>(InProp))
	{
		FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_touserdata(InL, InStackIndex);
		if (Wrapper && Wrapper->DelegateInst && Wrapper->bIsMulti)
		{
			MultiDelegateProp->SetPropertyValue_InContainer(InBuff, *(FMulticastScriptDelegate*)Wrapper->DelegateInst);
		}
	}
	else if (UArrayProperty* ArrayProp = Cast<UArrayProperty>(InProp))
	{
		FScriptArrayHelper ArrayHelper(ArrayProp, InBuff);
		int32 i = 0;
		lua_pushnil(InL);
		while (lua_next(InL, -2))
		{
			if (ArrayHelper.Num() < i + 1)
			{
				ArrayHelper.AddValue();
			}

			FetchProperty(InL, ArrayProp->Inner, ArrayHelper.GetRawPtr(i), -1);
			lua_pop(InL, 1);
			++i;
		}
	}

	else if (USetProperty* SetProp = Cast<USetProperty>(InProp))
	{
		FScriptSetHelper SetHelper(SetProp, InBuff);
		int32 i = 0;
		
		lua_pushnil(InL);
		while (lua_next(InL, -2))
		{
			if (SetHelper.Num() < i + 1)
			{
				SetHelper.AddDefaultValue_Invalid_NeedsRehash();
			}

			FetchProperty(InL, SetHelper.ElementProp, SetHelper.GetElementPtr(i), -1);
			++i;
			lua_pop(InL, 1);
		}

		SetHelper.Rehash();
	}
	else if (UMapProperty* MapProp = Cast<UMapProperty>(InProp))
	{
		FScriptMapHelper MapHelper(MapProp, InBuff);
		int32 i = 0;
		
		lua_pushnil(InL);
		while (lua_next(InL, -2))
		{
			if (MapHelper.Num() < i + 1)
			{
				MapHelper.AddDefaultValue_Invalid_NeedsRehash();
			}

			FetchProperty(InL, MapProp->KeyProp, MapHelper.GetPairPtr(i), -2);
			FetchProperty(InL, MapProp->ValueProp, MapHelper.GetPairPtr(i), -1);
			++i;
			lua_pop(InL, 1);
		}

		MapHelper.Rehash();
	}

	return ;

}

UObject* FUnrealMisc::FetchObject(lua_State* InL, int32 InIndex)
{
	InIndex = lua_absindex(InL, InIndex);

	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(InL, InIndex);
	if (Wrapper && Wrapper->ObjInst)
	{
		return Wrapper->ObjInst;
	}

	return nullptr;
}

void FUnrealMisc::PushObject(lua_State* InL, UObject* InObj)
{
	if (InObj == nullptr)
	{
		lua_pushnil(InL);
		return;
	}

	{
		FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_newuserdata(InL, sizeof(FLuaObjectWrapper));
		Wrapper->ObjInst = InObj;

		UClass* Class = InObj->GetClass();
		if (int32* Ref = FLuaUnrealWrapper::Get().RegisteredUClass.Find(FName(*Class->GetName())))
		{
			lua_rawgeti(InL, LUA_REGISTRYINDEX, *Ref);
			lua_setmetatable(InL, -2);
		}
	}
}


void* FUnrealMisc::FetchStruct(lua_State* InL, int32 InIndex)
{
	FLuaStructWrapper* Wrapper = (FLuaStructWrapper*)lua_touserdata(InL, InIndex);
	if (Wrapper)
	{
		return &(Wrapper->StructInst);
	}

	return nullptr;
}

void FUnrealMisc::PushStruct(lua_State* InL, UScriptStruct* InStruct, void* InBuff)
{
	int32 StructSize = FMath::Max(InStruct->GetStructureSize(), 1);
	FLuaStructWrapper* Wrapper = (FLuaStructWrapper*)lua_newuserdata(InL, sizeof(void*) + StructSize);
	Wrapper->StructType = InStruct;

	InStruct->CopyScriptStruct(&(Wrapper->StructInst), InBuff);

	if (int32* Ref = FLuaUnrealWrapper::Get().RegisteredUScriptStruct.Find(FName(*InStruct->GetName())))
	{
		lua_rawgeti(InL, LUA_REGISTRYINDEX, *Ref);
		lua_setmetatable(InL, -2);
	}
}

int32 FUnrealMisc::GetObjectProperty(lua_State* L)
{
	UProperty* Prop = (UProperty*)lua_touserdata(L, lua_upvalueindex(1));
	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(L, 1);
	PushProperty(L, Prop, Wrapper->ObjInst, !Prop->HasAnyPropertyFlags(CPF_BlueprintReadOnly));

	return 1;
}

int32 FUnrealMisc::SetObjectProperty(lua_State* L)
{
	UProperty* Prop = (UProperty*)lua_touserdata(L, lua_upvalueindex(1));
	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(L, 1);
	FetchProperty(L, Prop, Wrapper->ObjInst, 2);

	return 0;
}

int32 FUnrealMisc::GetStructProperty(lua_State* InL)
{
	UProperty* Prop = (UProperty*)lua_touserdata(InL, lua_upvalueindex(1));
	FLuaStructWrapper* Wrapper = (FLuaStructWrapper*)lua_touserdata(InL, 1);
	PushProperty(InL, Prop, Wrapper->StructInst, !Prop->HasAnyPropertyFlags(CPF_BlueprintReadOnly));
	return 1;
}

int32 FUnrealMisc::SetStructProperty(lua_State* InL)
{
	UProperty* Prop = (UProperty*)lua_touserdata(InL, lua_upvalueindex(1));
	FLuaStructWrapper* Wrapper = (FLuaStructWrapper*)lua_touserdata(InL, 1);
	FetchProperty(InL, Prop, Wrapper->StructInst, 2);

	return 0;
}

int32 FUnrealMisc::CallFunction(lua_State* L)
{
	UFunction* Func = (UFunction*)lua_touserdata(L, lua_upvalueindex(1));
	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(L, 1);

	if (Wrapper)
	{
		UObject* Obj = Wrapper->ObjInst;
		if (Obj)
		{
			return CallFunction_Impl(L, 2, Obj, Func);
		}
	}

	return 0;
}

int32 FUnrealMisc::CallStaticFunction(lua_State* L)
{
	UFunction* Func = (UFunction*)lua_touserdata(L, lua_upvalueindex(1));
	UObject* Obj = (UObject*)lua_touserdata(L, lua_upvalueindex(2));

	return CallFunction_Impl(L, 1, Obj, Func);
}

int32 CallFunction_Impl(lua_State* L, int32 StackTop, UObject* Obj, UFunction* Func)
{
	if (Func->Children == nullptr)
	{
		Obj->ProcessEvent(Func, nullptr);
		return 0;
	}
	else
	{
		FStructOnScope FuncParam(Func);
		UProperty* ReturnProp = nullptr;

		for (TFieldIterator<UProperty> It(Func); It; ++It)
		{
			UProperty* Prop = *It;
			if (Prop->HasAnyPropertyFlags(CPF_ReturnParm))
			{
				ReturnProp = Prop;
			}
			else
			{
				FUnrealMisc::FetchProperty(L, Prop, FuncParam.GetStructMemory(), StackTop++, FName(*Func->GetName()));
			}
		}

		Obj->ProcessEvent(Func, FuncParam.GetStructMemory());

		int32 ReturnNum = 0;
		if (ReturnProp)
		{
			FUnrealMisc::PushProperty(L, ReturnProp, FuncParam.GetStructMemory(), false);
			++ReturnNum;
		}

		if (Func->HasAnyFunctionFlags(FUNC_HasOutParms))
		{
			for (TFieldIterator<UProperty> It(Func); It; ++It)
			{
				UProperty* Prop = *It;
				if (Prop->HasAnyPropertyFlags(CPF_OutParm) && !Prop->HasAnyPropertyFlags(CPF_ConstParm))
				{
					FUnrealMisc::PushProperty(L, *It, FuncParam.GetStructMemory(), false);
					++ReturnNum;
				}
			}
		}

		return ReturnNum;
	}

}