// Fill out your copyright notice in the Description page of Project Settings.

#include "LuaScriptFunctionLibrary.h"
#include "LuaUnrealWrapper.h"
#include "FileHelper.h"

bool ULuaScriptFunctionLibrary::ResetLuaScript()
{
	FLuaUnrealWrapper::Get().Reset();
	return true;
}

FString ULuaScriptFunctionLibrary::RunLuaCode(const FString& InLuaCode)
{
	return FLuaUnrealWrapper::Get().DoLuaCode(InLuaCode);
}

bool ULuaScriptFunctionLibrary::RegisterLuaAPI(UGameInstance* InGameInstance)
{
	FLuaUnrealWrapper::Get().RegisterUnreal(InGameInstance);
	return true;
}

bool ULuaScriptFunctionLibrary::RunLuaMainFunction()
{
	FLuaUnrealWrapper::Get().RunMainFunction();
	return true;
}

FString ULuaScriptFunctionLibrary::LoadFileToString(const FString& InFileName)
{
	FString Ret;
	FFileHelper::LoadFileToString(Ret, *InFileName);
	return Ret;
}
