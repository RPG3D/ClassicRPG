// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "DelegateCallLua.generated.h"

struct lua_State;

/**
 * a UObject wrapper for UE4 and Lua function
 */
UCLASS()
class UDelegateCallLua : public UObject
{
	GENERATED_BODY()
public:
	UDelegateCallLua(const FObjectInitializer& ObjInit);

	//now, now conside One lua state for the plugin
	static lua_State* LuaState;

	//Lua function index in lua global registry
	int32 LuaFunctionID = 0;

	//single delegate or multi delegate
	bool bIsMulti;

	//raw delegate
	void* DelegateInst = nullptr;

	//the UFunction bound to this Delegate
	UFunction* FunctionSignature = nullptr;

	UFUNCTION(BlueprintCallable)
		void TestFunction() {}

	UFUNCTION(BlueprintCallable)
		static FName GetWrapperFunctionName() { return FName(TEXT("TestFunction")); }

	virtual void ProcessEvent(UFunction* InFunction, void* Parms) override;
};
