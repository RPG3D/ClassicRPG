﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "LuaUnrealWrapper.h"
#include "Package.h"
#include "LuaScript.h"
#include "lua.hpp"
#include "UnrealMisc.h"
#include "Engine/GameInstance.h"
#include "UObjectIterator.h"
#include "DelegateCallLua.h"
#include "FileHelper.h"
#include "Paths.h"
#include "PlatformFilemanager.h"
#include "LuaScriptConfig.h"
#include "LuaSocketWrap.h"

int InitUnrealLib(lua_State* L);
int RegisterRawAPI(lua_State* InL);
int PrintLog(lua_State* L);
int GetGameInstance(lua_State* L);
int LuaLoadObject(lua_State* Inl);
int LuaLoadClass(lua_State*);
int BindDelegate(lua_State* InL);
int UnbindDelegate(lua_State* InL);
int RequireFromUFS(lua_State* InL);
int LuaNewObject(lua_State* InL);
int LuaMakeStruct(lua_State* InL);

FLuaUnrealWrapper::FLuaUnrealWrapper()
{
}

FLuaUnrealWrapper::~FLuaUnrealWrapper()
{
}

FLuaUnrealWrapper& FLuaUnrealWrapper::Get()
{
	static FLuaUnrealWrapper Inst;
	if (Inst.L == nullptr)
	{
		Inst.Init();
	}

	return Inst;
}

void FLuaUnrealWrapper::Init()
{
	if (L == nullptr)
	{
		L = luaL_newstate();
		luaL_openlibs(L);
	}
}

void FLuaUnrealWrapper::RegisterProperty(lua_State* InL, const UProperty* InProperty)
{
	if (FUnrealMisc::IsScriptReadableProperty(InProperty) == false)
	{
		return;
	}

	{
		lua_pushlightuserdata(InL, (void*)InProperty);
		lua_pushcclosure(InL, FUnrealMisc::GetObjectProperty, 1);

		const FString PropName = "Get" + InProperty->GetName();
		lua_setfield(InL, -2, TCHAR_TO_UTF8(*PropName));
	}

	{
		lua_pushlightuserdata(InL, (void*)InProperty);
		lua_pushcclosure(InL, FUnrealMisc::SetObjectProperty, 1);

		const FString PropName = "Set" + InProperty->GetName();
		lua_setfield(InL, -2, TCHAR_TO_UTF8(*PropName));
	}
}

void FLuaUnrealWrapper::RegisterFunction(lua_State* InL, const UFunction* InFunction, const UClass* InClass)
{
	if (InClass == nullptr || FUnrealMisc::IsScriptCallableFunction(InFunction) == false)
	{
		return;
	}

	const FString FuncName = InFunction->GetName();
	const bool bIsStatic = InFunction->HasAnyFunctionFlags(FUNC_Static);

	if (bIsStatic)
	{
		lua_pushlightuserdata(InL, (void*)InFunction);
		lua_pushlightuserdata(InL, (((UClass*)InClass)->GetDefaultObject()));
		lua_pushcclosure(InL, FUnrealMisc::CallStaticFunction, 2);

		lua_setfield(InL, -2, TCHAR_TO_UTF8(*FuncName));
	}
	else
	{
		lua_pushlightuserdata(InL, (void*)InFunction);
		lua_pushcclosure(InL, FUnrealMisc::CallFunction, 1);

		lua_setfield(InL, -2, TCHAR_TO_UTF8(*FuncName));
	}
}

void FLuaUnrealWrapper::Reset()
{
	if (L)
	{
		lua_close(L);
		L = nullptr;
	}

	RegisteredUClass.Reset();
	RegisteredUScriptStruct.Reset();

	extern TMap<int32, UDelegateCallLua*> DelegateCallLuaList;

	for (TObjectIterator<UDelegateCallLua>It; It; ++It)
	{
		if ((*It)->bIsMulti && (*It)->DelegateInst)
		{
			FMulticastScriptDelegate* MultiDelegateInst = (FMulticastScriptDelegate*)((*It)->DelegateInst);
			if (MultiDelegateInst && MultiDelegateInst->IsBound())
			{
				MultiDelegateInst->Remove((*It), UDelegateCallLua::GetWrapperFunctionName());
			}
		}
		else if ((*It)->bIsMulti == false && (*It)->DelegateInst)
		{
			FScriptDelegate* SingleDelegateInst = (FScriptDelegate*)((*It)->DelegateInst);
			if (SingleDelegateInst && SingleDelegateInst->IsBound())
			{
				SingleDelegateInst->Clear();
			}
		}

		(*It)->RemoveFromRoot();
		DelegateCallLuaList.Remove((*It)->LuaFunctionID);
	}

	UDelegateCallLua::LuaState = nullptr;
}

void FLuaUnrealWrapper::RegisterUnreal(UGameInstance* InGameInst)
{
	CachedGameInstance = InGameInst;

	RegisterRawAPI(L);

	//create a table "Unreal" in _G
	luaL_requiref(L, "Unreal", InitUnrealLib, 1);
	int ret = lua_getglobal(L, "Unreal");

	int32 ModuleIndex = lua_gettop(L);
	for (TObjectIterator<UClass>It; It; ++It)
	{
		if (const UClass* Class = Cast<const UClass>(*It))
		{
			if (RegisterUClass(L, ModuleIndex, Class))
			{
				lua_pop(L, 1);
			}
		}
	}

	for (TObjectIterator<UScriptStruct>It; It; ++It)
	{
		if (const UScriptStruct* ScriptStruct = Cast<UScriptStruct>(*It))
		{
			if (RegisterUScriptStruct(L, ModuleIndex, ScriptStruct))
			{
				lua_pop(L, 1);
			}
		}
	}

	//register game instance
	{
		ret = lua_getglobal(L, "Unreal");
		FUnrealMisc::PushObject(L, InGameInst);
		lua_setfield(L, -2, "GameInstance");
	}

	//set package path
	{
		FString LuaEnvPath;
		ULuaScriptConfig* CfgObject = GetMutableDefault<ULuaScriptConfig>();
		for (int32 i = 0; i < CfgObject->LuaEnvRelativePaths.Num(); ++i)
		{
			LuaEnvPath += FPaths::ProjectDir() + CfgObject->LuaEnvRelativePaths[i].Path / FString("?.lua;");
			LuaEnvPath += FPaths::ProjectDir() + CfgObject->LuaEnvRelativePaths[i].Path / FString("?/init.lua;");
		}

		for (int32 i = 0; i < CfgObject->LuaEnvFullPaths.Num(); ++i)
		{
			LuaEnvPath += CfgObject->LuaEnvFullPaths[i].Path / FString("?.lua;");
			LuaEnvPath += CfgObject->LuaEnvFullPaths[i].Path / FString("?/init.lua;");
		}

		ret = lua_getglobal(L, "package");
		FString RetString = UTF8_TO_TCHAR(lua_pushstring(L, TCHAR_TO_UTF8(*LuaEnvPath)));
		lua_setfield(L, -2, "path");
		lua_pop(L, 1);
	}

	{
		int32 tp = lua_gettop(L);
		int32 ret = lua_getglobal(L, "package");
		lua_getfield(L, -1, "searchers");
		int32 FunctionNum = luaL_len(L, -1);
		lua_pushcfunction(L, RequireFromUFS);
		lua_rawseti(L, -2, FunctionNum + 1);
		lua_settop(L, tp);
	}

	//init static
	UDelegateCallLua::LuaState = L;

	//init lua socket
	slua::LuaSocket::init(L);
}


void FLuaUnrealWrapper::RunMainFunction()
{
	//载入入口脚本
	ULuaScriptConfig* CfgObject = GetMutableDefault<ULuaScriptConfig>();
	FString LoadMainScript = FString("require ('") + CfgObject->EntryFile.FilePath + FString("')");
	int32 Ret = luaL_dostring(GetLuaSate(), TCHAR_TO_UTF8(*LoadMainScript));
	if (Ret > 0)
	{
		FString RetString = UTF8_TO_TCHAR(lua_tostring(FLuaUnrealWrapper::Get().GetLuaSate(), -1));
		UE_LOG(LogLuaScript, Warning, TEXT("%s"), *RetString);
	}
	//执行入口函数Main
	Ret = lua_getglobal(FLuaUnrealWrapper::Get().GetLuaSate(), "Main");
	Ret = lua_pcall(FLuaUnrealWrapper::Get().GetLuaSate(), 0, 0, 0);
	if (Ret > 0)
	{
		FString RetString = UTF8_TO_TCHAR(lua_tostring(FLuaUnrealWrapper::Get().GetLuaSate(), -1));
		UE_LOG(LogLuaScript, Warning, TEXT("%s"), *RetString);
	}
}

bool FLuaUnrealWrapper::RegisterUClass(lua_State* InL, int32 InModuleIndex, const UClass* InClass)
{
	const FString ClassName = InClass->GetName();
	const char* ClassNameRaw = TCHAR_TO_UTF8(*ClassName);

	int32* ExistRef = RegisteredUClass.Find(FName(*ClassName));
	if (ExistRef)
	{
		lua_rawgeti(InL, LUA_REGISTRYINDEX, *ExistRef);
		return true;
	}

	if (FUnrealMisc::HasScriptAccessibleField(InClass) == false)
	{
		return false;
	}

	//create a table for UClass
	lua_newtable(InL);
	int32 ClassIndex = lua_gettop(InL);
	 
	//set the class table into _G[Unreal]
	lua_pushvalue(InL, ClassIndex);
	lua_setfield(InL, InModuleIndex, ClassNameRaw);

	int32 TP = lua_gettop(InL);

	//set the class table into global registry
	lua_pushvalue(InL, ClassIndex);
	int32 ClassRef = luaL_ref(InL, LUA_REGISTRYINDEX);

	RegisteredUClass.Add(FName(*ClassName), ClassRef);

	//table.__index = table, methods are stored in metatable
	lua_pushstring(InL, "__index");
	lua_pushvalue(InL, ClassIndex);
	lua_rawset(InL, ClassIndex);

	bool bHasSuper = false;
	if (const UClass* SuperClass = InClass->GetSuperClass())
	{
		bHasSuper = RegisterUClass(InL, InModuleIndex, SuperClass);
		if (bHasSuper)
		{
			//set parent class's table as the metatable for current class's table
			lua_setmetatable(InL, ClassIndex);
		}
	}
	else
	{
		//really need do this?
		//lua_pushvalue(InL, ClassIndex);
		//lua_setmetatable(InL, ClassIndex);
	}

	for (TFieldIterator<const UField> It(InClass, EFieldIteratorFlags::ExcludeSuper); It; ++It)
	{
		if (const UProperty* Prop = Cast<const UProperty>(*It))
		{
			RegisterProperty(InL, Prop);
			continue;
		}

		if (const UFunction* Func = Cast<const UFunction>(*It))
		{
			RegisterFunction(InL, Func, InClass);
			continue;
		}
	}

	return true;
}


bool FLuaUnrealWrapper::RegisterUScriptStruct(lua_State* InL, int32 InModuleIndex, const UScriptStruct* InScriptStruct)
{
	FString StructName = InScriptStruct->GetName();
	const char* RawName = TCHAR_TO_UTF8(*StructName);
	
	int32* ExistRef = RegisteredUScriptStruct.Find(FName(*StructName));
	if (ExistRef)
	{
		lua_rawgeti(InL, LUA_REGISTRYINDEX, *ExistRef);
		return true;
	}

	if (FUnrealMisc::HasScriptAccessibleField(InScriptStruct) == false)
	{
		return false;
	}

	lua_newtable(InL);
	int32 StructIndex = lua_gettop(InL);

	lua_pushvalue(InL, StructIndex);
	lua_setfield(InL, InModuleIndex, RawName);

	lua_pushvalue(InL, StructIndex);
	int32 StructRef = luaL_ref(InL, LUA_REGISTRYINDEX);
	RegisteredUScriptStruct.Add(FName(*StructName), StructRef);

	lua_pushstring(InL, "__index");
	lua_pushvalue(InL, StructIndex);
	lua_rawset(InL, StructIndex);

	bool bHasSuper = false;
	if (const UScriptStruct* SuperStruct = Cast<UScriptStruct>(InScriptStruct->GetSuperStruct()))
	{
		bHasSuper = RegisterUScriptStruct(InL, InModuleIndex, SuperStruct);
		if (bHasSuper)
		{
			//set parent class's table as the metatable for current class's table
			lua_setmetatable(InL, StructIndex);
		}
	}
	else
	{
		//really need do this?
		//lua_pushvalue(InL, StructIndex);
		//lua_setmetatable(InL, StructIndex);
	}

	for (TFieldIterator<const UField> It(InScriptStruct); It; ++It)
	{
		if (const UProperty* Prop = Cast<UProperty>(*It))
		{
			RegisterProperty(InL, Prop);
			continue;
		}
	}

	return true;
}

FString FLuaUnrealWrapper::DoLuaCode(const FString& InCode)
{
	luaL_dostring(FLuaUnrealWrapper::Get().GetLuaSate(), TCHAR_TO_UTF8(*InCode));
	return UTF8_TO_TCHAR(lua_tostring(L, -1));
}

int InitUnrealLib(lua_State* L)
{
	const static luaL_Reg funcs[] =
	{

	{"print", PrintLog},
	{"GetGameInstance", GetGameInstance},
	{"BindDelegate", BindDelegate},
	{"UnbindDelegate", UnbindDelegate},
	{"LuaLoadObject", LuaLoadObject},
	{"LuaLoadClass", LuaLoadClass},
	{"LuaNewObject", LuaNewObject},
	{"LuaMakeStruct", LuaMakeStruct},
	{nullptr, nullptr}

	};

	luaL_newlib(L, funcs);

	return 1;
}

int RegisterRawAPI(lua_State* InL)
{
	lua_getglobal(InL, "_G");
	const static luaL_Reg funcs[] =
	{
		{ "print", PrintLog },
		//{ "GetGameInstance", GetGameInstance },
		{ nullptr, nullptr }
	};

	luaL_setfuncs(InL, funcs, 0);

	lua_pop(InL, 1);

	return 0;
}

int PrintLog(lua_State* L)
{
	FString StringToPrint;
	int Num = lua_gettop(L);
	for (int i = 1; i <= Num; ++i)
	{
		StringToPrint.Append(UTF8_TO_TCHAR(lua_tostring(L, i)));
	}

	UE_LOG(LogLuaScript, Log, TEXT("%s"), *StringToPrint);
	return 0;
}

int GetGameInstance(lua_State* L)
{
	lua_getglobal(L, "Unreal");
	lua_getfield(L, -1, "GameInstance");
	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(L, -1);
	if (Wrapper && Wrapper->ObjInst)
	{
		if (UGameInstance* GameInst = Cast<UGameInstance>(Wrapper->ObjInst))
		{
			FUnrealMisc::PushObject(L, GameInst);
		}
		else
		{
			lua_pushnil(L);
		}
	}
	else
	{
		lua_pushnil(L);
	}

	return 1;
}

//lua functuion index<-->UE4's delegate proxy object
TMap<int32, UDelegateCallLua*> DelegateCallLuaList;

int BindDelegate(lua_State* InL)
{
	FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_touserdata(InL, -2);

	lua_pushvalue(InL, -1);
	if (lua_isfunction(InL, -1) == false || Wrapper == nullptr)
	{
		lua_pop(InL, 1);
		luaL_traceback(InL, InL, "Error in BindDelegate", 1);
		UE_LOG(LogLuaScript, Warning, TEXT("%s"), UTF8_TO_TCHAR(lua_tostring(InL, -1)));
		return 0;
	}

	UDelegateCallLua* DelegateObject = NewObject<UDelegateCallLua>();
	DelegateObject->LuaFunctionID = luaL_ref(InL, LUA_REGISTRYINDEX);
	DelegateObject->FunctionSignature = Wrapper->FunctionSignature;
	DelegateObject->bIsMulti = Wrapper->bIsMulti;

	if (Wrapper->bIsMulti)
	{
		FMulticastScriptDelegate* MultiDelegate = (FMulticastScriptDelegate*)Wrapper->DelegateInst;
		if (MultiDelegate)
		{
			DelegateObject->AddToRoot();
			FScriptDelegate ScriptDelegate;
			ScriptDelegate.BindUFunction(DelegateObject, UDelegateCallLua::GetWrapperFunctionName());
			MultiDelegate->Add(ScriptDelegate);
			DelegateObject->DelegateInst = MultiDelegate;
			DelegateCallLuaList.Add(DelegateObject->LuaFunctionID, DelegateObject);
		}
	}
	else
	{
		FScriptDelegate* SingleDelegate = (FScriptDelegate*)Wrapper->DelegateInst;
		if (SingleDelegate)
		{
			DelegateObject->AddToRoot();
			SingleDelegate->BindUFunction(DelegateObject, UDelegateCallLua::GetWrapperFunctionName());
			DelegateObject->DelegateInst = SingleDelegate;
			DelegateCallLuaList.Add(DelegateObject->LuaFunctionID, DelegateObject);
		}
	}

	//retuen lua function index in registry, useful when Unbind lua function from UE4's delegate
	lua_pushinteger(InL, DelegateObject->LuaFunctionID);
	return 1;
}

int UnbindDelegate(lua_State* InL)
{
	int32 tp = lua_gettop(InL);
	FLuaDelegateWrapper* Wrapper = (FLuaDelegateWrapper*)lua_touserdata(InL, -tp);

	if (Wrapper && Wrapper->bIsMulti)
	{
		if (FMulticastScriptDelegate* MultiDelegate = (FMulticastScriptDelegate*)Wrapper->DelegateInst)
		{
			//remove one
			if (tp > 1)
			{
				int32 LuaFuncID = lua_tointeger(InL, -1);

				UDelegateCallLua** CachedFuncObj = DelegateCallLuaList.Find(LuaFuncID);
				if (CachedFuncObj && (*CachedFuncObj)->DelegateInst == MultiDelegate)
				{
					//remove bind
					MultiDelegate->Remove(*CachedFuncObj, UDelegateCallLua::GetWrapperFunctionName());
					//after Unbind, remove lua function from registry
					// bind one lua function to 2 diff UE4's delegate will get 2 diff lua function Index in registry
					luaL_unref(InL, LUA_REGISTRYINDEX, LuaFuncID);
					(*CachedFuncObj)->RemoveFromRoot();
					DelegateCallLuaList.Remove(LuaFuncID);
					(*CachedFuncObj)->ConditionalBeginDestroy();
				}

			}
			//remove all
			else
			{
				for (TObjectIterator<UDelegateCallLua>It; It; ++It)
				{
					if ((*It)->DelegateInst == MultiDelegate)
					{
						//remove bind
						MultiDelegate->Remove(*It, UDelegateCallLua::GetWrapperFunctionName());
						luaL_unref(InL, LUA_REGISTRYINDEX, (*It)->LuaFunctionID);
						(*It)->RemoveFromRoot();
						DelegateCallLuaList.Remove((*It)->LuaFunctionID);
						(*It)->ConditionalBeginDestroy();
					}
					
				}
			}

		}
	}
	else if (Wrapper)
	{
		if (FScriptDelegate* SingleDelegate = (FScriptDelegate*)Wrapper->DelegateInst)
		{
			//remove one
			if (tp > 1)
			{
				int32 LuaFuncID = lua_tointeger(InL, -1);

				UDelegateCallLua** CachedFuncObj = DelegateCallLuaList.Find(LuaFuncID);
				if (CachedFuncObj && (*CachedFuncObj)->DelegateInst == SingleDelegate)
				{
					SingleDelegate->Clear();
					//after Unbind, remove lua function from registry
					// bind one lua function to 2 diff UE4's delegate will get 2 diff lua function Index in registry
					luaL_unref(InL, LUA_REGISTRYINDEX, LuaFuncID);
					(*CachedFuncObj)->RemoveFromRoot();
					DelegateCallLuaList.Remove(LuaFuncID);
					(*CachedFuncObj)->ConditionalBeginDestroy();
				}

			}
		}
	}
	return 0;
}

//LuaLoadObject(Owner, ObjectPath)
int LuaLoadObject(lua_State* InL)
{
	int32 tp = lua_gettop(InL);
	if (tp < 2)
	{
		return 0;
	}

	FString ObjectPath = UTF8_TO_TCHAR(lua_tostring(InL, 2));

	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(InL, 1);

	UObject* LoadedObj = LoadObject<UObject>(Wrapper ? Wrapper->ObjInst : nullptr, *ObjectPath);
	if (LoadedObj == nullptr)
	{
		UE_LOG(LogLuaScript, Warning, TEXT("LoadObject failed: %s"), *ObjectPath);
		return 0;
	}
	FUnrealMisc::PushObject(InL, LoadedObj);

	return 1;
}

//LuaLoadClass(Owner, ClassPath)
int LuaLoadClass(lua_State* InL)
{
	int32 tp = lua_gettop(InL);
	if (tp < 2)
	{
		return 0;
	}

	FString ClassPath = UTF8_TO_TCHAR(lua_tostring(InL, 2));

	if (UClass* FoundClass = FindObject<UClass>(ANY_PACKAGE, *ClassPath))
	{
		FUnrealMisc::PushObject(InL, FoundClass);
		return 1;
	}

	FLuaObjectWrapper* Wrapper = (FLuaObjectWrapper*)lua_touserdata(InL, 1);
	UClass* LoadedClass = LoadObject<UClass>(Wrapper ? Wrapper->ObjInst : nullptr, *ClassPath);
	if (LoadedClass == nullptr)
	{
		UE_LOG(LogLuaScript, Warning, TEXT("LoadClass failed: %s"), *ClassPath);
		return 0;
	}

	tp = lua_gettop(InL);
	lua_getglobal(InL, "Unreal");
	FLuaUnrealWrapper::Get().RegisterUClass(InL, tp + 1, LoadedClass);
	lua_settop(InL, tp);
	FUnrealMisc::PushObject(InL, LoadedClass);

	return 1;
}

int RequireFromUFS(lua_State* InL)
{
	const char* RawFilwName = lua_tostring(InL, -1);
	FString FileName = UTF8_TO_TCHAR(RawFilwName);

	FileName.ReplaceInline(*FString("."), *FString("/"));

	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();
	lua_getglobal(InL, "package");
	lua_getfield(InL, -1, "path");
	FString LuaSearchPath = UTF8_TO_TCHAR(lua_tostring(InL, -1));
	TArray<FString> PathList;
	LuaSearchPath.ParseIntoArray(PathList, TEXT(";"));

	for (int32 i = 0; i < PathList.Num(); ++i)
	{
		FString FullFilePath = PathList[i].Replace(TEXT("?"), *FileName);
		FullFilePath = FullFilePath.Replace(TEXT("\\"), TEXT("\\"));

		FPaths::NormalizeFilename(FullFilePath);

		IFileHandle* LuaFile = PlatformFile.OpenRead(*FullFilePath);
		if (LuaFile)
		{
			TArray<uint8> FileData;
			FileData.Init(0, LuaFile->Size());
			if (FileData.Num() < 1)
			{
				continue;
			}
			LuaFile->Read(FileData.GetData(), FileData.Num());
			
			int32 BomLen = 0;
			if (FileData.Num() > 2 &&
				FileData[0] == 0xEF &&
				FileData[1] == 0xBB &&
				FileData[2] == 0xBF)
			{
				BomLen = 3;
			}

			FString RetPath = FString("@") + FullFilePath;
			int ret = luaL_loadbuffer(InL, (const char*)FileData.GetData() + BomLen, FileData.Num() - BomLen, TCHAR_TO_UTF8(*RetPath));
			//return full file path as 2nd value, useful for some debug tool 
			lua_pushstring(InL, TCHAR_TO_UTF8(*FullFilePath));
			if (ret != LUA_OK)
			{
				UE_LOG(LogLuaScript, Warning, TEXT("%s"), UTF8_TO_TCHAR(lua_tostring(InL, -1)));
			}

			delete LuaFile;
			return 2;
		}
	}

	FString MessageToPush = FString("RequireFromUFS failed: ") + FileName;
	luaL_traceback(InL, InL, TCHAR_TO_UTF8(*MessageToPush), 1);
	UE_LOG(LogLuaScript, Warning, TEXT("%s"), UTF8_TO_TCHAR(lua_tostring(InL, -1)));
	return 0;
}

//LuaNewObject(Owner, ClassName, ObjectName)
int LuaNewObject(lua_State* InL)
{
	int ParamNum = lua_gettop(InL);
	if (ParamNum < 3)
	{
		//error
		return 0;
	}

	UObject* ObjOwner = nullptr;
	FLuaObjectWrapper* OwnerWrapper = (FLuaObjectWrapper*)lua_touserdata(InL, 1);
	if (OwnerWrapper && OwnerWrapper->ObjInst)
	{
		ObjOwner = OwnerWrapper->ObjInst;
	}
	else
	{
		return 0;
	}

	FString ClassName = UTF8_TO_TCHAR(lua_tostring(InL, 2));


	FString ObjName = UTF8_TO_TCHAR(lua_tostring(InL, 3));
	UClass* ObjClass = FindObject<UClass>(ANY_PACKAGE, *ClassName);
	if (ObjClass == nullptr)
	{
		return 0;
	}
	UObject* NewObj = NewObject<UObject>(ObjOwner, ObjClass, FName(*ObjName));
	FUnrealMisc::PushObject(InL, NewObj);
	return 1;
}

//LuaMakeStruct(StructName)
int LuaMakeStruct(lua_State* InL)
{
	if (lua_gettop(InL) < 1)
	{
		return 0;
	}


	return 0;
}