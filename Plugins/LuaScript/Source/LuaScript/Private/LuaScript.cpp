// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "LuaScript.h"
#include "LuaScriptConfig.h"
#include "Modules/ModuleManager.h"
#include "Paths.h"
#include "App.h"

#if WITH_EDITOR
#include "ISettingsModule.h"
#include "ISettingsSection.h"
#endif

#define LOCTEXT_NAMESPACE "FLuaScriptModule"

void FLuaScriptModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
	
#if WITH_EDITOR
	ISettingsModule* SettingsModule = FModuleManager::GetModulePtr<ISettingsModule>("Settings");
	if (SettingsModule == nullptr)
	{
		return;
	}

	ISettingsSectionPtr SettingSection = SettingsModule->RegisterSettings(FName("Project"), FName("Plugins"), FName("LuaScript"),
		FText::FromString("LuaScript"), FText::FromString("LuaScript"), GetMutableDefault<ULuaScriptConfig>());

	if (SettingSection.IsValid())
	{
		SettingSection->OnModified().BindRaw(this, &FLuaScriptModule::HandleSettingSaved);


	}
#endif

}

void FLuaScriptModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

bool FLuaScriptModule::HandleSettingSaved()
{
	ULuaScriptConfig* CfgObject = GetMutableDefault<ULuaScriptConfig>();

	FString OldPath = GetMutableDefault<ULuaScriptConfig>()->EntryFile.FilePath;
	CfgObject->EntryFile.FilePath = FPaths::GetBaseFilename(OldPath);

	FString ProjectRootPath = FPaths::ProjectDir();

	for (int32 i = 0; i < CfgObject->LuaEnvRelativePaths.Num(); ++i)
	{
		FString Path = CfgObject->LuaEnvRelativePaths[i].Path;
		if (Path.StartsWith(ProjectRootPath))
		{
			Path.ReplaceInline(*ProjectRootPath, *FString(""));
		}

		CfgObject->LuaEnvRelativePaths[i].Path = Path;
	}

	return true;
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FLuaScriptModule, LuaScript)

DEFINE_LOG_CATEGORY(LogLuaScript);