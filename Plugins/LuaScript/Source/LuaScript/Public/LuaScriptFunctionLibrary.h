// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "LuaScriptFunctionLibrary.generated.h"

/**
 * 
 */
UCLASS()
class LUASCRIPT_API ULuaScriptFunctionLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
		static bool ResetLuaScript();

	//make sure you have called RegisterLuaAPI()
	UFUNCTION(BlueprintCallable)
		static FString RunLuaCode(const FString& InLuaCode);

	//call RegisterLuaAPI() first!!!
	UFUNCTION(BlueprintCallable)
		static bool RegisterLuaAPI(class UGameInstance* InGameInstance);

	//make sure you have called RegisterLuaAPI()
	UFUNCTION(BlueprintCallable)
		static bool RunLuaMainFunction();

	UFUNCTION(BlueprintCallable)
		static FString LoadFileToString(const FString& InFileName);
};
