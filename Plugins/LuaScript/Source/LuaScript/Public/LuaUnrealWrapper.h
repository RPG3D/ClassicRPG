﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

struct lua_State;

/**
 * this is the Entry class for the plugin
 */
class LUASCRIPT_API FLuaUnrealWrapper
{
public:

	~FLuaUnrealWrapper();

	static FLuaUnrealWrapper& Get();

	void Reset();

	lua_State* GetLuaSate() { return L; }

	FString DoLuaCode(const FString& InCode);

	//export all UObject and FStruct to lua state, if InScriptPath is nul, use config path
	void RegisterUnreal(class UGameInstance* InGameInstance);

	void RunMainFunction();

	//register a UClass to lua
	bool RegisterUClass(lua_State* InL, int32 InModuleIndex, const UClass* InClass);
	//register a FStruct to lu
	bool RegisterUScriptStruct(lua_State* InL, int32 InModuleIndex, const UScriptStruct* InScriptStruct);

	//lua has to hold a long life time Uobject, GameInstance is a right one 
	class UGameInstance* CachedGameInstance = nullptr;
protected:
	FLuaUnrealWrapper();
	FLuaUnrealWrapper(const FLuaUnrealWrapper&) = delete;

	//init lua state and std lib
	void Init();

	void RegisterProperty(lua_State* InL, const class UProperty* InProperty);
	void RegisterFunction(lua_State* InL, const class UFunction* InFunction, const class UClass* InClass = nullptr);

	lua_State * L = nullptr;
public:
	TMap<FName, int32> RegisteredUClass;
	TMap<FName, int32> RegisteredUScriptStruct;
};
