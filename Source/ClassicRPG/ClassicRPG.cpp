// Fill out your copyright notice in the Description page of Project Settings.

#include "ClassicRPG.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ClassicRPG, "ClassicRPG" );

DEFINE_LOG_CATEGORY(LogClassicRPG);
