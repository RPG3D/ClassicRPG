// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RPGInstance.generated.h"


/**
 * 
 */
UCLASS()
class CLASSICRPG_API URPGInstance : public UGameInstance
{
	GENERATED_BODY()
public:

	virtual void Init() override;

protected:

};
