// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "ClassicRPGEvent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnFrameTick, float, InDeltaTime);

/**
 * 
 */
UCLASS()
class CLASSICRPG_API UClassicRPGEvent : public UObject
{
	GENERATED_BODY()
public:
	
	UPROPERTY(BlueprintAssignable)
		FOnFrameTick FrameTick;
};
