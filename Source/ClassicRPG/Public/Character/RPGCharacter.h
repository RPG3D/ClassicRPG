// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AbilitySystemInterface.h"
#include "Abilities/GameplayAbility.h"
#include "Ability/RPGAttributeSet.h"
#include "RPGCharacter.generated.h"

UCLASS()
class CLASSICRPG_API ARPGCharacter : public ACharacter, public IAbilitySystemInterface
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ARPGCharacter();

	virtual void PossessedBy(AController* NewController) override;
	virtual void UnPossessed() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintCallable)
		virtual UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	UFUNCTION(BlueprintCallable)
		FGameplayAbilitySpecHandle GiveAbilityByClassName(const FString& InAbilityClassName);

	UFUNCTION(BlueprintCallable)
		bool TryActiveAbilityByHandle(FGameplayAbilitySpecHandle InAbilitySpecHandle);

	//Attribute
	UFUNCTION(BlueprintCallable)
		virtual int32 GetIntAttribute(FName InName) const;

	UFUNCTION(BlueprintCallable)
		virtual void SetIntAttribute(FName InName, int32 InValue);

	UFUNCTION(BlueprintCallable)
		virtual bool HasAttribute(FName InName) const;

protected:

	UFUNCTION(BlueprintImplementableEvent)
		void OnDamaged(float InDamageAmount, const FHitResult& HitInfo, const struct FGameplayTagContainer& InDamageTags, ACharacter* InCharacter, AActor* InDamageCauser);

	UFUNCTION(BlueprintImplementableEvent)
		void OnAttributeChanged(FName InName, float InValue, const FGameplayTagContainer& InEventTags);


protected:

	virtual void HandleDamage(float InDamageAmount, const FHitResult& InHitInfo, const struct FGameplayTagContainer& InDamageTags, ACharacter* InCharacter, AActor* InDamageCauser);
	virtual void HandleAttributeChanged(FName InName, float InValue, const struct FGameplayTagContainer& InEventTags);


	friend URPGAttributeSet;
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UAbilitySystemComponent* AbilitySystemComponent = nullptr;

	UPROPERTY()
		URPGAttributeSet* AttributeSet = nullptr;
};
