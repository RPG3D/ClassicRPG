// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameEvent/ClassicRPGEvent.h"
#include "RPGSingleton.generated.h"

class UClassicRPGEvent;
class UGameInstance;

/**
 * 
 */
UCLASS()
class CLASSICRPG_API URPGSingleton : public UObject
{
	GENERATED_BODY()
public:

	UFUNCTION(BlueprintCallable)
		virtual void Init(UGameInstance* InGameInst);
	UFUNCTION(BlueprintCallable)
		UClassicRPGEvent* GetGameEvent() { return RPGEvent; }

protected:
	bool Tick(float InDeltaTime)
	{ 
		if (RPGEvent)
		{
			RPGEvent->FrameTick.Broadcast(InDeltaTime);
		}
		return true; 
	}

	TWeakObjectPtr<UGameInstance> GameInst = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UClassicRPGEvent* RPGEvent = nullptr;
	
};
