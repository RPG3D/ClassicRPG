// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayEffectExecutionCalculation.h"
#include "RPGGameplayEffectCalculation.generated.h"

/**
 * 
 */
UCLASS()
class CLASSICRPG_API URPGGameplayEffectCalculation : public UGameplayEffectExecutionCalculation
{
	GENERATED_BODY()
public:
	
	URPGGameplayEffectCalculation();

	virtual void Execute_Implementation(const FGameplayEffectCustomExecutionParameters& InExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecution) const override;
	
};
