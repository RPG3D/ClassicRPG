// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "GameplayEffectTypes.h"
#include "Abilities/GameplayAbilityTargetTypes.h"
#include "AbilityTypes.generated.h"

/**
 * 
 */
USTRUCT(BlueprintType)
struct FEffectContainer
{
	GENERATED_BODY()
public:
	FEffectContainer() {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<UObject> TargetType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<UGameplayEffect>> TargetGameplayEffectClasses;
};

USTRUCT(BlueprintType)
struct FEffectContainerSpec
{
	GENERATED_BODY()
public:
	FEffectContainerSpec() {}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		FGameplayAbilityTargetDataHandle TargetData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FGameplayEffectSpecHandle> TargetEffectSpecs;

	bool HasValidEffects() const;

	bool HasValidTarget() const;

	void AddTargets(const TArray<FHitResult>& InHitResults, const TArray<AActor*>& InTargetActors);
};