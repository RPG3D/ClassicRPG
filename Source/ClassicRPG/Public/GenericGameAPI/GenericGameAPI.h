// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "GenericGameAPI.generated.h"

/**
 * 
 */
UCLASS()
class CLASSICRPG_API UGenericGameAPI : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintCallable)
		static class URPGSingleton* GetRPGSingleton();

	UFUNCTION(BlueprintCallable)
		static bool HackNonePakLimit();

};