// Fill out your copyright notice in the Description page of Project Settings.

#include "GenericGameAPI.h"
#include "ClassicRPG.h"
#include "GameSingleton/RPGSingleton.h"
#include "Engine.h"
#include "PackageName.h"
#include "PlatformFilemanager.h"
#include "Misc/DateTime.h"
#include "GameEvent/ClassicRPGEvent.h"
#include "GameSingleton/RPGSingleton.h"
#include "IPlatformFilePak.h"

class URPGSingleton* UGenericGameAPI::GetRPGSingleton()
{
	if (URPGSingleton* Singleton = Cast<URPGSingleton>(GEngine->GameSingleton))
	{
		return Singleton;
	}

	return nullptr;
}

bool UGenericGameAPI::HackNonePakLimit()
{
	IPlatformFile* PlatformFile = &FPlatformFileManager::Get().GetPlatformFile();
	FString CurrentPlatformFileName = FString(PlatformFile->GetName());
	FString PakPlatformFileName = FPakPlatformFile::GetTypeName();
	if (CurrentPlatformFileName.Equals(PakPlatformFileName, ESearchCase::IgnoreCase))
	{
		// On Windows 64, ExcludedNonPakExtensions offset is 0x50;
		// On Android 32 offset is 0x1c
		// On iOS offset is 0x68
		// 
		int32 Offset = 0x50;
#if PLATFORM_WINDOWS
		Offset = 0x50;
#elif PLATFORM_ANDROID
		Offset = 0x1c;
#elif PLATFORM_IOS
		Offset = 0x68;
#endif
		FPakPlatformFile* PakPlatformFile = (FPakPlatformFile*)PlatformFile;
		uint8* RawPtr = (uint8*)PlatformFile;
		TSet<FName>* ExcludedNonPakExtensions = (TSet<FName>*)(RawPtr + Offset);

		TArray<FName> PackageExt = ExcludedNonPakExtensions->Array();
		for (int32 i = 0; i < PackageExt.Num(); ++i)
		{
			UE_LOG(LogTemp, Warning, TEXT("ExcludedNonPakExtensions: %s"), *PackageExt[i].ToString());
		}

		FFilenameSecurityDelegate& PakCheckDelegate = FPakPlatformFile::GetFilenameSecurityDelegate();
		UE_LOG(LogTemp, Warning, TEXT("FFilenameSecurityDelegate IsBound : %d"), (int32)PakCheckDelegate.IsBound());

		if (PackageExt.Num())
		{
			ExcludedNonPakExtensions->Reset();
		}

		ExcludedNonPakExtensions = (TSet<FName>*)(RawPtr + Offset);

		PackageExt = ExcludedNonPakExtensions->Array();
		if (PackageExt.Num())
		{
			UE_LOG(LogTemp, Log, TEXT("**************Hack ExcludedNonPakExtensions Failed*********************"));
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("*****************Hack ExcludedNonPakExtensions OK**********************"));
		}
	}
	return true;
}
