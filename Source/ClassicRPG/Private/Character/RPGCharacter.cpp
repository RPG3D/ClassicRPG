// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGCharacter.h"
#include "AbilitySystemComponent.h"

// Sets default values
ARPGCharacter::ARPGCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AbilitySystemComponent = CreateDefaultSubobject<UAbilitySystemComponent>(TEXT("AbilitySystemComponent"));
	AttributeSet = CreateDefaultSubobject<URPGAttributeSet>(TEXT("AttributeSet"));
}

void ARPGCharacter::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);

	if (AbilitySystemComponent)
	{
		AbilitySystemComponent->InitAbilityActorInfo(this, this);
	}
}

void ARPGCharacter::UnPossessed()
{

}

// Called when the game starts or when spawned
void ARPGCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARPGCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ARPGCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

UAbilitySystemComponent* ARPGCharacter::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

FGameplayAbilitySpecHandle ARPGCharacter::GiveAbilityByClassName(const FString& InAbilityClassName)
{
	if (AbilitySystemComponent == nullptr)
	{
		return FGameplayAbilitySpecHandle();
	}

	UClass* AbilityClassObj = FindObject<UClass>(ANY_PACKAGE, *InAbilityClassName);
	if (AbilityClassObj == nullptr)
	{
		AbilityClassObj = LoadObject<UClass>(nullptr, *InAbilityClassName);
	}
	if (AbilityClassObj && AbilityClassObj->IsChildOf<UGameplayAbility>())
	{
		return AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(Cast<UGameplayAbility>(AbilityClassObj->GetDefaultObject())));
	}

	return FGameplayAbilitySpecHandle();
}

bool ARPGCharacter::TryActiveAbilityByHandle(FGameplayAbilitySpecHandle InAbilitySpecHandle)
{
	if (AbilitySystemComponent)
	{
		return AbilitySystemComponent->TryActivateAbility(InAbilitySpecHandle);
	}

	return false;
}

int32 ARPGCharacter::GetIntAttribute(FName InName) const
{

	return 0;
}

void ARPGCharacter::SetIntAttribute(FName InName, int32 InValue)
{

}

bool ARPGCharacter::HasAttribute(FName InName) const
{
	return false;
}

void ARPGCharacter::HandleDamage(float InDamageAmount, const FHitResult& InHitInfo, const struct FGameplayTagContainer& InDamageTags, ACharacter* InCharacter, AActor* InDamageCauser)
{
	OnDamaged(InDamageAmount, InHitInfo, InDamageTags, InCharacter, InDamageCauser);
}

void ARPGCharacter::HandleAttributeChanged(FName InName, float InValue, const struct FGameplayTagContainer& InEventTags)
{
	OnAttributeChanged(InName, InValue, InEventTags);
}

