// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGInstance.h"
#include "ClassicRPG.h"
#include "GameSingleton/RPGSingleton.h"
#include "Engine/Engine.h"

void URPGInstance::Init()
{
	Super::Init();

	if (URPGSingleton* Singleton = Cast<URPGSingleton>(GEngine->GameSingleton))
	{
		Singleton->Init(this);
	}

}
