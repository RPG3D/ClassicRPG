// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGGameplayEffectCalculation.h"
#include "RPGAttributeSet.h"

struct RPGDamageStatics
{
	DECLARE_ATTRIBUTE_CAPTUREDEF(DefensePower);
	DECLARE_ATTRIBUTE_CAPTUREDEF(AttackPower);
	DECLARE_ATTRIBUTE_CAPTUREDEF(Damage);

	RPGDamageStatics()
	{
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, DefensePower, Target, false);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, AttackPower, Source, true);
		DEFINE_ATTRIBUTE_CAPTUREDEF(URPGAttributeSet, Damage, Source, true);
	}
};

static const RPGDamageStatics& DamageStatics()
{
	static RPGDamageStatics DmgStatics;
	return DmgStatics;
}


URPGGameplayEffectCalculation::URPGGameplayEffectCalculation()
{
	RelevantAttributesToCapture.Add(DamageStatics().DefensePowerDef);
	RelevantAttributesToCapture.Add(DamageStatics().AttackPowerDef);
	RelevantAttributesToCapture.Add(DamageStatics().DamageDef);
}

void URPGGameplayEffectCalculation::Execute_Implementation(const FGameplayEffectCustomExecutionParameters& InExecutionParams, OUT FGameplayEffectCustomExecutionOutput& OutExecution) const
{
	UAbilitySystemComponent* TargetAbilityComponent = InExecutionParams.GetTargetAbilitySystemComponent();
	UAbilitySystemComponent* SourceAbilityComponent = InExecutionParams.GetSourceAbilitySystemComponent();

	AActor* SourceActor = SourceAbilityComponent ? SourceAbilityComponent->AvatarActor : nullptr;
	AActor* TargetActor = TargetAbilityComponent ? TargetAbilityComponent->AvatarActor : nullptr;

	const FGameplayEffectSpec& Spec = InExecutionParams.GetOwningSpec();

	const FGameplayTagContainer* SourceTags = Spec.CapturedSourceTags.GetAggregatedTags();
	const FGameplayTagContainer* TargetTags = Spec.CapturedTargetTags.GetAggregatedTags();

	FAggregatorEvaluateParameters EvaluationParams;
	EvaluationParams.SourceTags = SourceTags;
	EvaluationParams.TargetTags = TargetTags;

	// Damage Done = Damage * AttackPower / DefensePower
	// if DefensePower is 0, it is treated as 1.f

	float DefensePower = 1.f;
	InExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().DefensePowerDef, EvaluationParams, DefensePower);
	if (DefensePower == 0.f)
	{
		DefensePower = 1.f;
	}

	float AttackPower = 0.f;
	InExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().DamageDef, EvaluationParams, AttackPower);

	float Damage = 0.f;
	InExecutionParams.AttemptCalculateCapturedAttributeMagnitude(DamageStatics().DamageDef, EvaluationParams, Damage);

	float DamageDone = Damage * AttackPower / DefensePower;

	if (DamageDone > 0.f)
	{
		OutExecution.AddOutputModifier(FGameplayModifierEvaluatedData(DamageStatics().DamageProperty, EGameplayModOp::Additive, DamageDone));
	}
}
