// Fill out your copyright notice in the Description page of Project Settings.

#include "RPGSingleton.h"
#include "Engine/GameInstance.h"

void URPGSingleton::Init(UGameInstance* InGameInst)
{
	GameInst = InGameInst;
	if (RPGEvent == nullptr)
	{
		RPGEvent = NewObject<UClassicRPGEvent>(this, FName(TEXT("RPGEvent")));
		FTickerDelegate TickDelegate;
		TickDelegate.BindUObject(this, &URPGSingleton::Tick);
		FTicker::GetCoreTicker().AddTicker(TickDelegate);
	}
}
